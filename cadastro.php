<!DOCTYPE <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Cadastro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <script src="main.js"></script>-->
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/theme.css" type="text/css">
</head>

<body>
    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <div class="w-100 py-2">
        <div class="container">
            <div class="row">
                <div class="col-md-12 bg-primary">
                    <ul class="nav nav-pills">
                        <li class="nav-item"> <a href="index.html" class="active nav-link"> <i class="fa fa-home fa-home"></i>&nbsp;Home</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="py-2 my-2">
        <div class="container">
            <div class="row">
                <div class="col-md-12 bg-primary"></div>
            </div>
        </div>
    </div>
    <div class="">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="row my-2">
                        <div class="col-md-12 my-1 bg-primary">
                            <h3 class="text-center my-1">Tecnologias</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center align-items-center my-3">
                <div class="col-md-12">
                    <textarea required class="form-control w-100" rows="1" name="txtDescricao" id="txtDescricao" style="margin-top: 0px; margin-bottom: 0px; height: 42px;"
                        placeholder="Descrição da tecnologia"></textarea>
                </div>
            </div>
        </div>
    </div>

    <div class="w-100">
        <div class="container">
            <div class="row">
                <button class="btn btn-primary flex-column ml-auto" onclick="">Gravar</button>
            </div>
        </div>
        <div class="py-2 my-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 bg-primary"></div>
                </div>
            </div>
        </div>
        <div class="">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="row my-2">
                            <div class="col-md-12 my-1 bg-primary">
                                <h3 class="text-center my-1">Tipo de Objeto</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <script src="js/form.js"></script>
                <div class="">
                    <div class="dropdown">
                        <div class="dropdown">
                            <button required class="btn btn-defaut dropdown-toggle" id="btnTecnologia" style="position:relative;"
                                type="button" data-toggle="dropdown">Escolha uma Tecnologia <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a onclick="selectTecnologia(this.innerText)" class="dropdown-item" href="#">Exemplo1</a></li>
                                <li><a onclick="selectTecnologia(this.innerText)" class="dropdown-item" href="#">Exemplo2</a></li>
                                <li><a onclick="selectTecnologia(this.innerText)" class="dropdown-item" href="#">Exemplo3</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row text-center align-items-center my-3">
                    <div class="col-md-12">
                        <textarea required class="form-control w-100" rows="1" name="txtDescricao" id="txtDescricao"
                            style="margin-top: 0px; margin-bottom: 0px; height: 42px;" placeholder="Descrição do tipo de desenvolvimento"></textarea>
                    </div>
                </div>
                <div class="">Tipo de Objeto<br><br>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="rdNovo" name="rdNovo" class="custom-control-input" checked>
                        <label class="custom-control-label" for="rdNovo">Novo</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="rdModificacao" name="rdNovo" class="custom-control-input">
                        <label class="custom-control-label" for="rdModificacao">Modificação</label>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <button class="btn btn-primary flex-column ml-auto" onclick="">Gravar</button>
                    </div>
                </div>
            </div>
        </div>

</body>

</html>